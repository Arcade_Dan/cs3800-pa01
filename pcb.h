/*

    I have decided to create my own pseudo Process Control Block for
    the sake of readability and organization.


*/


#pragma once
#include "process.h"
#include <queue>
#include <utility>
#include <deque>
using std::queue;
using std::tuple;
using std::deque;



struct TaskScheduler
{

    deque<Process*> *ready_procs = nullptr;
    deque<Process*> *blocked_procs = nullptr;
    queue<IOEvent*> IOqueue;

    TaskScheduler(deque<Process*> &rp, deque<Process*> &bp);


};


struct ProcessControlBlock
{

    vector<Process> *proclist = nullptr;
    Process *cur_proc = nullptr;
    deque<Process*> ready_procs;
    deque<Process*> blocked_procs;
    TaskScheduler *scheduler = nullptr;
    
    long proc_time;

    
    ~ProcessControlBlock();
    

    ProcessControlBlock(string &file, vector<Process> &plist);
   
    // tracks time unit(s) in main 
    void timeSet(long &time);
    //checks all processes for their ready state
    void checkReady();
    // checks current process needs to be blocked
    void checkBlock();
    // assigns the first ready process in queue into cur_process
    void getFirstProcess();
    //checks states
    void checkStates();
    // runs the process
    void runProcess();

    void getBP();
    //checks if process is done
    void checkDone();
    

};

