#include "pcb.h"



TaskScheduler::TaskScheduler(deque<Process*> &rp, deque<Process*> &bp)
{
    blocked_procs = &bp;
    ready_procs = &rp;

}


ProcessControlBlock::ProcessControlBlock(string &file, vector<Process> &plist)
{
    initProcessSetFromFile(file, plist);
    proclist = &plist;
    scheduler = new TaskScheduler(ready_procs, blocked_procs);
    
}

void ProcessControlBlock::timeSet(long &time)
{
    proc_time = time;
}

void ProcessControlBlock::checkReady()
{
    for (Process & i : *proclist)
    {
        if (i.arrivalTime == proc_time)
        {
                i.state = ready;
                // places ready processes onto the queue
                ready_procs.push_back(&i);
        }

    }
}

void ProcessControlBlock::getFirstProcess()
{

    if (cur_proc == nullptr)
    {
        cur_proc = ready_procs.front();
        ready_procs.pop_front();
        if (!cur_proc->ioEvents.empty())
        {
            for (IOEvent & i : cur_proc->ioEvents)
            {
                if (i.duration != 0)
                {
                    cur_proc->currentEvent = i;
                }

            }
        }
        
        cur_proc->state = processing;
    }
}

void ProcessControlBlock::checkStates()
{
    checkReady();
    if (cur_proc == nullptr)
    {
        getFirstProcess();

    }
    checkBlock();
    getBP();
    checkDone();

}

void ProcessControlBlock::checkBlock()
{

    if ((cur_proc->state == processing) && (cur_proc->processorTime == cur_proc->currentEvent.time) && cur_proc != nullptr)
    {
        cur_proc->state = blocked;

        blocked_procs.push_back(cur_proc);
        ready_procs.pop_front();
        cur_proc = nullptr;
        
    }
}

void ProcessControlBlock::checkDone()
{
    if (cur_proc != nullptr)
    { 
        if (cur_proc->processorTime >= cur_proc->reqProcessorTime)
        {
            
            cur_proc->state = done;
            cur_proc = nullptr;
        }
    }
}

void ProcessControlBlock::runProcess()
{   
    if (cur_proc != nullptr)
    {
        if (cur_proc->state == ready)
            cur_proc->state = processing;

        if (cur_proc != nullptr && cur_proc->state == processing)
        {
            cur_proc->processorTime++;
        }

        if (!blocked_procs.empty())
        {
            for (Process* & i : blocked_procs)
            {
                i->currentEvent.duration--;
            }
        }
    }

}

void ProcessControlBlock::getBP()
{
    for (Process* & i : blocked_procs)
    {
        if (i->currentEvent.duration <= 0)
        {
            i->state = ready;
            
        }
    }
}

ProcessControlBlock::~ProcessControlBlock()
{
    
    
    proclist = nullptr;
    cur_proc = nullptr;

}
